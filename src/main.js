var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var particles = []
var numOfParticles = 3
var particlePush = setInterval(pushParticle, 1)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < numOfParticles; i++) {
    particles.push(new Particle())
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  for (var i = 0; i < particles.length; i++) {
    particles[i].display()
    particles[i].move()

    if (particles[i].popIt()) {
      particles.splice(i, 1)
    }
  }
  pop()
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function pushParticle() {
  for (var i = 0; i < numOfParticles; i++) {
    particles.push(new Particle())
  }
}

class Particle {
  constructor() {
    this.dir = Math.random() * 2
    this.speed = 0.001 + Math.random() * 0.0025
    this.size = 0.25
    this.dist = 0
    this.grow = 0.0005
    this.color = 1
    this.fade = 0.01
  }

  move() {
    this.dist += this.speed
    this.size -= this.grow
    this.color -= this.fade
  }

  display() {
    fill(this.color * 255)
    noStroke()
    ellipse(sin(Math.PI * this.dir) * this.dist * boardSize, cos(Math.PI * this.dir) * this.dist * boardSize, sin(this.size) * boardSize)
  }

  popIt() {
    if (this.dist > 0.35) {
      return true
    }
  }
}
